const express = require("express")

const app = express()

// Middleware for parsing JSON body requests
app.use(express.json())

// Headers for bypassing CORS policy and allowing communications between domains
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  res.header("Access-Control-Allow-Credentials", "true")
  next()
})

module.exports = app
