const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")
const Apartment = require("db/apartments/collection")
const _ = require("underscore")
const sharp = require("sharp")

const router = express.Router()
const jwtSecurity = require("core/jwt")
// const digitalOcean = require("lib/services/digitalOcean")

const { inspect } = require("util")

const Busboy = require("busboy")
const config = require("config")
const path = require("path")
const os = require("os")
const fs = require("fs")
const security = require("./security")
const queryOptimizer = require("./queryOptimizer")

router.post("/create", [
  jwtSecurity.checkToken,
], async (req, res, next) => {
  try {
    return solveForm(req, async (data) => {
      const name = data?.name
      if (!name) return res.status(400).json({ message: "No name" })

      const description = data?.description
      if (!description) return res.status(400).json({ message: "No description" })

      const images = data?.imageSources
      if (!images) return res.status(400).json({ message: "No images" })

      // creating the apartment
      await new Apartment(data).save()

      return res.json({ message: "Apartment created" })
    })
  } catch (error) {
    return errorService.handle({
      error, res, next, message: "Error on apartments route /create",
    })
  }
})

// router.get("/get-image", async (req, res, next) => {
//   try {
//     const Key = req?.query?.Key
//     const downloadUrl = digitalOcean.createDownloadSignedUrl(Key)

//     return res.redirect(downloadUrl)
//   } catch (error) {

//     return errorService.handle({
//       error, res, next, message: "Error on apartments route /get-image",
//     })
//   }
// })

router.get("/", [
  security.list,
  queryOptimizer.optimizeFilters,
  queryOptimizer.optimizeOptions,
], async (req, res, next) => {
  try {
    const { filters } = req
    const { options } = req
    const projection = {
      _id: 1, name: 1, description: 1, imageSources: 1,
    }

    const apartments = await Apartment.find(filters, projection, options)
    const total = await Apartment.countDocuments(filters)
    return res.json({ apartments, total })
  } catch (error) {
    return errorService.handle({
      error, res, next, message: "Error on apartments route /",
    })
  }
})

router.patch("/update", [
  security.update,
], async (req, res, next) => {
  try {
    const apartmentId = req?.query?.apartmentId
    const data = req?.body

    if (!apartmentId) return res.status(400).json({ message: "No apartment Id" })
    const canUpdate = await getPermission(userId, permissionsActions.UDDATE, permissionsEntities.APARTMENTS)
    if (!canUpdate) return res.status(400).json({ message: "Not authorised to update" })

    await Apartment.findByIdAndUpdate(apartmentId, { $set: data })

    return res.json({ message: "Apartment updated" })
  } catch (error) {
    return errorService.handle({
      error, res, next, message: "Error on apartments route /update",
    })
  }
})

router.delete("/delete", [
  security.remove,
], async (req, res, next) => {
  try {
    const apartmentId = req?.body?.apartmentId
    if (!apartmentId) return res.status(400).json({ message: "No apartment Id" })

    const canDelete = await getPermission(userId, permissionsActions.DELETE, permissionsEntities.APARTMENTS)
    if (!canDelete) return res.status(400).json({ message: "Not authorised to delete" })

    await Apartment.deleteOne({ _id: apartmentId })

    return res.json({ message: "Apartment deleted" })
  } catch (error) {
    return errorService.handle({
      error, res, next, message: "Error on apartments route /delete",
    })
  }
})

router.get("/single", [
  security.single,
], async (req, res, next) => {
  try {
    const apartmentId = req?.query?.apartmentId
    if (!apartmentId) return res.status(400).json({ message: "No apartment Id" })

    const apartment = await Apartment.findById(apartmentId, { name: 1, description: 1, imageSources: 1 })
    if (!apartment) return res.status(404).json({ message: "No apartment found" })

    res.json(apartment)
  } catch (error) {
    return errorService.handle({
      error, res, next, message: "Error on requests route /single",
    })
  }
})

router.get("/me", [
  security.me,
  queryOptimizer.optimizeFilters,
  queryOptimizer.optimizeOptions,
], async (req, res, next) => {
  try {
    const userId = req?.session?.userId
    const { filters } = req
    const { options } = req
    const projection = { _id: 1 }

    _.extend(filters, {
      userId: mongoose.Types.ObjectId(userId),
    })

    const apartments = await Apartment.find({ userId }, projection, options)
    const total = await Apartment.countDocuments(filters)

    res.json({ apartments, total })
  } catch (error) {
    return errorService.handle({
      error, res, next, message: "Error on apartments route /me",
    })
  }
})

const solveForm = async (req, clb) => {
  let counter = 0
  let didFieldParsingEnd = false
  const imageSources = []
  const data = {}

  const checkParsingFinished = () => {
    if (didFieldParsingEnd && !counter) {
      const createdBy = req?.decoded?.userId

      _.extend(data, { imageSources, createdBy })
      clb(data)
    }
  }

  const busboy = new Busboy({ headers: req.headers, limits: { fieldSize: 10000 } })
  busboy.on("file", (fieldname, file, filename) => {
    counter++
    const saveTo = path.join(os.tmpdir(), filename)
    file.pipe(fs.createWriteStream(saveTo))

    file.on("end", async () => {
      const croppedFilename = path.join(os.tmpdir(), `cropped-${filename}`)

      sharp(saveTo)
        .resize(500, 500)
        .toFile(croppedFilename, async (err) => {
          if (err) throw err

          const file = await fs.readFileSync(croppedFilename)
          const res = await digitalOcean.uploadFile(filename, file)
          imageSources.push(`${config.domain}/api/apartments/get-image?Key=${res?.Key}`)
          counter--

          checkParsingFinished()
        })
    })
  })

  busboy.on("field", (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) => {
    data[fieldname] = val
  })

  busboy.on("finish", () => {
    didFieldParsingEnd = true
    checkParsingFinished()
  })

  req.pipe(busboy)
}

module.exports = router
