const _ = require("underscore")

const optimizeFilters = (req, res, next) => {
  req.filters = {}

  return next()
}

const optimizeOptions = (req, res, next) => {
  // by default the filters are empty
  req.options = {}

  const limit = Number(req?.query?.limit) || 10
  // _.extend(req.options, { limit })

  const page = req?.query?.page || 1
  const skip = (page - 1) * limit
  _.extend(req.options, { skip })

  return next()
}

module.exports = {
  optimizeFilters,
  optimizeOptions,
}
