const create = (req, res, next) => next()
const list = (req, res, next) => next()
const update = (req, res, next) => next()
const remove = (req, res, next) => next()
const single = (req, res, next) => next()
const me = (req, res, next) => next()

module.exports = {
  create,
  list,
  update,
  remove,
  single,
  me,
}
