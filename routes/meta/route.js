const express = require("express")
const errorService = require("lib/errors/handler")

const createRouter = () => {
  const router = express.Router()
  router.get("/", async (req, res, next) => {
    try {
      return res.json({ message: "Ok" })
    } catch (error) {
      return errorService.handle({
        error, res, next, message: "Error on main route /",
      })
    }
  })

  return router
}

module.exports = createRouter
