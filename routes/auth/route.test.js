const mongoose = require("mongoose")
const User = require("db/users/collection")
const { AuthMethod } = require("db/auth-methods/collection")
const app = require("middleware/app")
const { responses } = require("db/auth-methods/constants")

const supertest = require("supertest")
const { authMethodTypes } = require("../../db/auth-methods/constants")

const authRouter = require("routes/auth/route")()

const request = supertest(app)

describe("auth route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await User.deleteMany({})
    await AuthMethod.deleteMany({})
  })

  it("password signup without email should not pass", async () => {
    app.use("/auth", authRouter)

    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
    }

    const res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)

    expect(res.body.message).toBe(responses.NO_EMAIL)
  })

  it("password signup without password should not pass", async () => {
    app.use("/auth", authRouter)

    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
      email: "admin@app.com",
    }

    const res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)

    expect(res.body.message).toBe(responses.NO_PASSWORD)
  })

  it("password signup with same data twice should fail", async () => {
    app.use("/auth", authRouter)

    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
      email: "admin@app.com",
      password: "danPot9_ij",
    }

    await request.post("/auth/signup/password")
      .send(data)
      .expect(200)

    const res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)

    expect(res.body.message).toBe(responses.EMAIL_ALREADY_USED)
  })

  it("signup test with valid data should pass", async () => {
    app.use("/auth", authRouter)

    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
      email: "user@app.com",
      password: "danPot9_ij",
    }

    const res = await request.post("/auth/signup/password")
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.ACCOUNT_CREATED)
  })

  it("password signup with invalid email should not pass", async () => {
    app.use("/auth", authRouter)

    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
      email: "admin",
    }

    const res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)

    expect(res.body.message).toBe(responses.EMAIL_INVALID)
  })

  it("password signup with weak password should not pass", async () => {
    app.use("/auth", authRouter)

    // short password
    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
      email: "admin@app.com",
      password: "111",
    }

    let res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)
    expect(res.body.message).toBe(responses.PASSWORD_WEAK)

    // no uppercase letters
    data.password = "mn00555555"
    res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)
    expect(res.body.message).toBe(responses.PASSWORD_WEAK)

    // no symbols
    data.password = "Mnsds990833"
    res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)
    expect(res.body.message).toBe(responses.PASSWORD_WEAK)

    // no numbers
    data.password = "Mnsdsd_&^"
    res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)
    expect(res.body.message).toBe(responses.PASSWORD_WEAK)
  })

  it("password signup with same email in different letter case twice should fail", async () => {
    app.use("/auth", authRouter)

    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
      email: "admin@app.com",
      password: "danPot9_ij",
    }

    await request.post("/auth/signup/password")
      .send(data)
      .expect(200)

    data.email = "Admin@App.Com"
    const res = await request.post("/auth/signup/password")
      .send(data)
      .expect(400)

    expect(res.body.message).toBe(responses.EMAIL_ALREADY_USED)
  })

  it("complete signup and signin should work", async () => {
    app.use("/auth", authRouter)

    const credentials = {
      email: "admin@app.com",
      password: "danPot9_ij",
    }

    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
      ...credentials,
    }

    await request.post("/auth/signup/password")
      .send(data)
      .expect(200)

    const res = await request.post("/auth/signin/password")
      .send(credentials)
      .expect(200)

    expect(res.body.message).toBe(responses.SIGNIN_SUCCESS)
  })

  it("signin with wrong password should not work", async () => {
    app.use("/auth", authRouter)

    const credentials = {
      email: "admin@app.com",
      password: "danPot9_ij",
    }

    const data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
        birthdate: new Date(),
      },
      ...credentials,
    }

    await request.post("/auth/signup/password")
      .send(data)
      .expect(200)

    const res = await request.post("/auth/signin/password")
      .send({
        email: "admin@app.com",
        password: "danPot9_im",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.PASSWORD_WRONG)
  })
})
