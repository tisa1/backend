const { PasswordAuthMethod } = require("db/auth-methods/collection")
const { responses } = require("db/auth-methods/constants")
const { isEmail, isStrongPassword } = require("validator")

const signupPassword = async (req, res, next) => {
  const email = req?.body?.email
  const password = req?.body?.password

  if (!email) return res.status(400).json({ message: responses.NO_EMAIL })
  if (!isEmail(email)) {
    return res.status(400).json({ message: responses.EMAIL_INVALID })
  }
  if (!password) return res.status(400).json({ message: responses.NO_PASSWORD })
  if (!isStrongPassword(password, {
    minLength: 8,
    minLowercase: 1,
    minUppercase: 1,
    minNumbers: 1,
    minSymbols: 1,
  })) return res.status(400).json({ message: responses.PASSWORD_WEAK })

  const existingAuthMethod = await PasswordAuthMethod.findOne({
    "credentials.email": email,
  })

  if (existingAuthMethod) return res.status(400).json({ message: responses.EMAIL_ALREADY_USED })

  return next()
}

const signinPassword = async (req, res, next) => {
  const email = req?.body?.email
  const password = req?.body?.password

  if (!email) return res.status(400).json({ message: responses.NO_EMAIL })
  if (!isEmail(email)) {
    return res.status(400).json({ message: responses.EMAIL_INVALID })
  }

  if (!password) return res.status(400).json({ message: responses.NO_PASSWORD })
  const authMethod = await PasswordAuthMethod.findOne({ "credentials.email": email }, { _id: 1 })
  if (!authMethod) return res.status(490).json({ message: responses.ACCOUNT_NOT_FOUND })

  return next()
}

module.exports = {
  signupPassword,
  signinPassword,
}
