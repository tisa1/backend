const express = require("express")
const errorService = require("lib/errors/handler")

const jwt = require("jsonwebtoken")
const config = require("config")
const bcrypt = require("bcryptjs")

const User = require("db/users/collection")
const { PasswordAuthMethod } = require("db/auth-methods/collection")
const jwtSecurity = require("core/jwt")
const { authMethodTypes, responses } = require("db/auth-methods/constants")
const routeSecurity = require("./security")

const createRouter = () => {
  const authRouter = express.Router()

  authRouter.post("/signup/password", [
    routeSecurity.signupPassword,
  ], async (req, res, next) => {
    try {
      const email = req?.body?.email
      const password = req?.body?.password

      const user = await new User(req?.body).save()
      await new PasswordAuthMethod({
        type: authMethodTypes.PASSWORD,
        userId: user._id,
        credentials: { email, password },
      }).save()

      const token = jwt.sign(
        { email, userId: user?._id },
        config.jwt.secret,
        {
          expiresIn: config.jwt.expiresIn,
        },
      )
      return res.json({ token, message: responses.ACCOUNT_CREATED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on auth register",
      })
    }
  })

  authRouter.post("/signin/password", [
    routeSecurity.signinPassword,
  ], async (req, res, next) => {
    try {
      const email = req?.body?.email
      const password = req?.body?.password
      const authMethod = await PasswordAuthMethod.findOne({ "credentials.email": email }, { "credentials.password": 1, userId: 1, })

      if (!bcrypt.compareSync(password, authMethod.credentials.password)) {
        return res.status(400).json({ message: responses.PASSWORD_WRONG })
      }

      const token = jwt.sign(
        /* eslint-disable */
        { email, userId: authMethod.userId },
        config.jwt.secret,
        {
          expiresIn: config.jwt.expiresIn,
        },
      )
      
      return res.json({ token, message: responses.SIGNIN_SUCCESS })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on auth login",
      })
    }
  })

  // authRouter.post("/exists", async (req, res, next) => {
  //   try {
  //     const email = req?.body?.email
  //     if (!email) return res.status(490).json({ message: "No email provided" })

  //     const user = await User.countDocuments({ email })

  //     return res.json(!!user)
  //   } catch (error) {
  //     return errorService.handle({
  //       req, res, next, message: "Error on auth login",
  //     })
  //   }
  // })

  // authRouter.get("/identity", jwtSecurity.checkToken, (req, res, next) => {
  //   try {
  //     return res.json(req.decoded)
  //   } catch (error) {
  //     return errorService.handle({
  //       req, res, next, message: "Error on auth identity",
  //     })
  //   }
  // })

  return authRouter
}

module.exports = createRouter
