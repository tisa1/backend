const logger = require("core/logger")
const GenericError = require("./Generic")

const handle = ({
  error, res, next, message,
}) => {
  if (error instanceof GenericError) {
    res.status(400).json({ message: error.message })
    return next()
  }
  res.status(500).json({ message: "An error occured" })
  logger.error({ message, data: error })
  return next()
}

module.exports = { handle }
