const mongoose = require("mongoose")

// Mongoose Schema for Apartment
const ApartmentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  createdBy: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  imageSources: {
    type: [String],
    required: true,
  },
},
{ timestamps: true })

// Define pre-hooks for the save method
ApartmentSchema.pre("save", (next) => {
  next()
})

module.exports = {
  ApartmentSchema,
}
