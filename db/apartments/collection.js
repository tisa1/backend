const mongoose = require("mongoose")
const { ApartmentSchema } = require("./schema")

const Apartment = mongoose.model("Apartment", ApartmentSchema)

module.exports = Apartment
