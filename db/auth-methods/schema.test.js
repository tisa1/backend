const mongoose = require("mongoose")
const { AuthMethod } = require("db/auth-methods/collection")
const { authMethodTypes } = require("db/auth-methods/constants")

describe("auth-methods", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await AuthMethod.deleteMany({})
  })

  it("should create an auth method", async () => {
    const data = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      method: authMethodTypes.PASSWORD,
      email: "test@app.com",
      password: "1111",

    }

    await new AuthMethod(data).save()
    const num = await AuthMethod.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert an empty auth method", async () => {
    const fakeAuthMethod = {}
    let error = null
    try {
      await new AuthMethod(fakeAuthMethod).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
