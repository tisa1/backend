const mongoose = require("mongoose")
const { AuthSchema, PasswordAuthSchema } = require("./schema")

const AuthMethod = mongoose.model("auth-methods", AuthSchema)

const PasswordAuthMethod = AuthMethod.discriminator("PasswordAuth", PasswordAuthSchema)

module.exports = { AuthMethod, PasswordAuthMethod }
