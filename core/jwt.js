const jwt = require("jsonwebtoken")
const config = require("config.js")

const checkToken = (req, res, next) => {
  const token = req.headers?.authorization?.split(" ")?.[1]
  if (!token) return res.status(403).json({ message: "No Bearer Token provided." })

  return jwt.verify(token, config.jwt.secret, (err, decoded) => {
    if (err) {
      return res.status(403).json({ message: "Token not valid" })
    }

    const email = decoded?.email
    if (!email) return res.status(403).json({ message: "The token provided is not registered for a user. To get an integration token use the /auth/register or /auth/login endpoints" })
    // Token is valid

    req.decoded = decoded
    return next()
  })
}

const checkIntegrationToken = (req, res, next) => {
  const token = req.headers?.authorization?.split(" ")?.[1]
  if (!token) return res.status(490).json({ message: "No Bearer Token provided." })

  return jwt.verify(token, config.jwt.secret, (err, decoded) => {
    if (err) {
      return res.status(490).json({ message: "Token not valid" })
    }
    const integrationId = decoded?.integrationId
    if (!integrationId) return res.status(490).json({ message: "The token provided is not registered under an integration. To get an integration token use the /integrations/register or /integrations/token endpoints" })
    // Token is valid

    req.decoded = decoded
    return next()
  })
}

module.exports = {
  checkToken,
  checkIntegrationToken,
}
